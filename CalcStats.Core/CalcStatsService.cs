﻿using System;

namespace CalcStats
{
    public class CalcStatsService
    {
        private int[] _array { get; init; }

        public int Count => _array.Length;

        public CalcStatsService(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array), "Value could not be null");
            }
            if (array.Length == 0)
            {
                throw new ArgumentException("Array could not be empty", nameof(array.Length));
            }
            _array = array;
        }

        public int GetMin()
        {
            int min = _array[0];
            foreach (int number in _array)
            {
                if (number < min)
                {
                    min = number;
                }
            }
            return min;
        }

        public int GetMax()
        {
            int max = _array[0];
            foreach (int number in _array)
            {
                if (number > max)
                {
                    max = number;
                }
            }
            return max;
        }

        public float GetAverage()
        {
            int sum = 0;
            foreach (int number in _array)
            {
                sum += number;
            }
            return sum / _array.Length;
        }

        public StatsData GetStatistic()
        {
            return new StatsData
            {
                Min = GetMin(),
                Max = GetMax(),
                Count = Count,
                Average = GetAverage()
            };
        }
    }
}
