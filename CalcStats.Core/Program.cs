﻿using System;

namespace CalcStats
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] array = new int[] { 1, 2, 2, 5, 22, -10, -6, 25, 9 };
            try
            {
                CalcStatsService service = new CalcStatsService(array);
                StatsData data = service.GetStatistic();
                ShowStatistic(data);
            }
            catch (ArgumentNullException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"[ERROR]: {ex.Message}");
            }

            Console.ReadKey();
        }

        public static void ShowStatistic(StatsData data)
        {
            Console.WriteLine("Statistic:");
            Console.WriteLine($"    Min: {data.Min}");
            Console.WriteLine($"    Max: {data.Max}");
            Console.WriteLine($"    Length: {data.Count}");
            Console.WriteLine($"    Average: {data.Average}");
        }
    }
}