﻿namespace CalcStats
{
    public class StatsData
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public int Count { get; set; }
        public float Average { get; set; }
    }
}
