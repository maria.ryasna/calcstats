using NUnit.Framework;
using System;

namespace CalcStats
{
    [TestFixture]
    public class CalcStatsTests
    {
        private CalcStatsService _service;

        [SetUp]
        public void SetUp()
        {
            int[] array = new int[] { 1, 2, -2, 4, 7, 6 };
            _service = new CalcStatsService(array);
        }

        [Test]
        public void GetMin_ShouldReturnMinValue()
        {
            // Arrange
            int expected = -2;

            // Act
            int actual = _service.GetMin();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetMax_ShouldReturnMaxValue()
        {
            // Arrange
            int expected = 7;

            // Act
            int actual = _service.GetMax();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetLength_ShouldReturnLength()
        {
            // Arrange
            int expected = 6;

            // Act
            int actual = _service.Count;

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetAverage_ShouldReturnAverageValue()
        {
            // Arrange
            float expected = 3;

            // Act
            float actual = _service.GetAverage();

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetStatistic_ShouldReturnStatsData()
        {
            // Arrange
            StatsData expected = new StatsData { Average = 3, Count = 6, Max = 7, Min = -2 };

            // Act
            StatsData actual = _service.GetStatistic();

            // Assert
            Assert.IsInstanceOf<StatsData>(actual);
            Assert.AreEqual(expected.Average, actual.Average);
            Assert.AreEqual(expected.Count, actual.Count);
            Assert.AreEqual(expected.Max, actual.Max);
            Assert.AreEqual(expected.Min, actual.Min);
        }

        [Test]
        public void Constructor_NullInsteadOfArray_ShouldReturnArgumentNullException()
        {
            // Arrange
            CalcStatsService service;

            // Act
            TestDelegate actual = () => {
                service = new CalcStatsService(null);
            };

            // Assert
            Assert.Throws<ArgumentNullException>(actual);
        }

        [Test]
        public void Constructor_EmptyArray_ShouldReturnArgumentException()
        {
            // Arrange
            int[] emptyArray = new int[] { };
            CalcStatsService service;

            // Act
            TestDelegate actual = () => {
                service = new CalcStatsService(emptyArray);
            };

            // Assert
            Assert.Throws<ArgumentException>(actual);
        }
    }
}